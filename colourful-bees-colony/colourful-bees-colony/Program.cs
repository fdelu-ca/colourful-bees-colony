﻿using colourful_bees_colony.beesColony;
using colourful_bees_colony.graph;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace colourful_bees_colony
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //Input params
            /*
             Задача раскрашивания графа (300 вершин, степень вершины не более 50, но не менее 1)
            классический пчелиный алгоритм (число пчел 60 из них 5 разведчика). 
            
            Зафиксировать качество полученного решения (значение целевой функции) после каждых 20 итераций до 1000 
            и построить график зависимости качества решения от числа итераций.
            */


            int cntVertex = 300;
            int cntEdgeMin = 1;
            int cntEdgeMax = 50;
            //Задан граф и степінь його вершин.
            Graph graph = GraphBuilder.GetRandomGraph(cntVertex,cntEdgeMin,cntEdgeMax);
            //graph.Print();

            int cntScout = 5;
            int cntForagers = 55;
            int iterations = 1000;
            //Задано загальне число бджіл і число розвідників.
            Hive hive = new Hive(cntScout, cntForagers);


            //Есть палитра всех доступных цветов и палитра используемых.

            Console.WriteLine("ABC Start...");
            DateTime dtStart = DateTime.Now;

            hive.Start(graph, iterations);

            DateTime dtStop = DateTime.Now;
            Console.WriteLine("ABC Ended...");
            Console.WriteLine("-----------------------------------------------------------");

            Console.WriteLine($"Start - {dtStart} | Stop - {dtStop} | DurationMs - {(dtStop - dtStart).TotalMilliseconds}");
            Console.WriteLine("ABC Solution :");
            hive.PrintResult();
        }
    }
}
