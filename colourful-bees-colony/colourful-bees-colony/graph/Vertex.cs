﻿using System.Drawing;

namespace colourful_bees_colony.graph
{
    public class Vertex
    {
        public Color Color;
        public int Id;
        public Vertex(int id)
        {
            Id = id;
        }
    }
}