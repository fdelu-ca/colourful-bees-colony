﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace colourful_bees_colony.graph
{
    /// <summary>
    /// Граф
    /// </summary>
    public class Graph : ICloneable
    {
        /// <summary>
        /// Список вершин графа
        /// </summary>

        public readonly int VertexCount = 0;
        readonly Vertex[] vertices;
        readonly bool[,] edges;

        public Graph(int cntVertex)
        {
            VertexCount = cntVertex;
            vertices = new Vertex[VertexCount];
            edges = new bool[VertexCount, VertexCount];

            for (int i = 0; i < cntVertex; i++)
            {
                vertices[i] = new Vertex(i);
            }
        }

        public bool SetEdge(int vertexA, int vertexB, bool val)
        {
            int maxVertex, maxLength;
            if (vertexA > vertexB)
                maxVertex = vertexA;
            else
                maxVertex = vertexB;

            if (edges.GetLength(0) > edges.GetLength(1))
                maxLength = edges.GetLength(0);
            else
                maxLength = edges.GetLength(1);

            if (vertexA != vertexB && maxVertex < maxLength)
            {
                edges[vertexA, vertexB] = val;
                edges[vertexB, vertexA] = val;
                return true;
            }
            return false;
        }

        internal void Print()
        {
            Console.WriteLine("Vertices:");
            Console.Write("    - ");
            for (int i = 0; i < edges.GetLength(0); i++)
                Console.Write(i + " ");
            Console.WriteLine();

            for (int i = 0; i < edges.GetLength(0); i++)
            {
                Console.Write(i.ToString("000") + " - ");
                for (int j = 0; j < edges.GetLength(0); j++)
                    Console.Write(Convert.ToInt32(edges[i, j]) + " ");
                Console.WriteLine();
            }

            Console.WriteLine("Edges Count:");
            for (int i = 0; i < vertices.Length; i++)
                Console.WriteLine(i.ToString("000") + " - " + GetEdgeCount(FindVertex(i)).ToString("000") + ((GetEdgeCount(FindVertex(i)) > 50) ? "!!!!!!!!!!!!!" : ""));

        }

        public List<Vertex> GetVertexEdges(Vertex index)
        {
            List<Vertex> edgesIds = new List<Vertex>();
            for (int i = 0; i < edges.GetLength(0); i++)
                if (edges[index.Id, i])
                    edgesIds.Add(vertices[i]);

            for (int i = 0; i < edges.GetLength(1); i++)
                if (edges[i, index.Id])
                    if (!edgesIds.Contains(vertices[i]))
                        edgesIds.Add(vertices[i]);

            return edgesIds;

        }

        internal List<Color> AllVertexColors()
        {
            List<Color> colors = new List<Color>();
            foreach (Vertex v in vertices)
                if (!colors.Contains(v.Color))
                    colors.Add(v.Color);

            return colors;
        }

        public int GetEdgeCount(Vertex index)
        {
            return GetVertexEdges(index).Count;
        }

        public Vertex FindVertex(int vertexId)
        {
            return vertices[vertexId];
        }

        public bool NeighborHaveThisColor(Vertex vertex, Color color)
        {
            List<Vertex> neighbors = GetVertexEdges(vertex);
            foreach (Vertex neighbor in neighbors)
                if (neighbor.Color == color)
                    return true;

            return false;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}