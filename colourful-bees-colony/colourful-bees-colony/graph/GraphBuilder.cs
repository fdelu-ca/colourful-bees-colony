﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace colourful_bees_colony.graph
{
    public class GraphBuilder
    {
        public GraphBuilder() { }

        public static Graph GetRandomGraph(int CountVertex, int MinEdge, int MaxEdge)
        {
            Graph graph = new Graph(CountVertex);
            Random random = new();

            //Поочерёдное заполнение вершин связями
            for (int mainVert = 0; mainVert < CountVertex; mainVert++)
            {
                //Генерация связей
                int cntEdge = random.Next(MinEdge, MaxEdge);
                //Перечень посещённых вершин
                List<int> vertVisited = new List<int>();

                cntEdge -= graph.GetEdgeCount(graph.FindVertex(mainVert));
                do
                {
                    //Защита от зацикливания
                    if (vertVisited.Count >= CountVertex)
                        break;

                    //Выбор точки
                    int subVert = random.Next(0, CountVertex);
                    if (vertVisited.Contains(subVert))
                        continue;
                    vertVisited.Add(subVert);

                    //Проверка на существование связи
                    if (graph.GetVertexEdges(graph.FindVertex(mainVert)).Contains(graph.FindVertex(subVert)))
                        continue;

                    //Связи вершиной вершины максимальны
                    if (graph.GetEdgeCount(graph.FindVertex(subVert)) >= MaxEdge)
                        continue;

                    //Добавление связи
                    bool addResult = graph.SetEdge(mainVert, subVert, true);
                    if (addResult)
                        cntEdge--;

                } while (cntEdge > 0);
            }
            return graph;
        }

        public static Vertex GetRandVertex(Graph graph)
        {
            Random random = new();
            int randId = random.Next(0, graph.VertexCount);
            Vertex vertex = graph.FindVertex(randId);
            return vertex;
        }

        public static Color GetRandColor()
        {
            Random rand = new Random();
            Color color = Color.FromArgb(rand.Next(1, 256), rand.Next(1, 256), rand.Next(1, 256));
            return color;
        }
    }
}
