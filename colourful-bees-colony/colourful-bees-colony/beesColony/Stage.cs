﻿namespace colourful_bees_colony.beesColony
{
    public enum Stage
    {
        Wait,
        Selected,
        Processed,
    }
}