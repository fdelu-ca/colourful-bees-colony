﻿using colourful_bees_colony.graph;
using System;

namespace colourful_bees_colony.beesColony
{
    public class Bee
    {
        public BeeType Type;
        public Vertex Vertex;

        public Bee(BeeType type)
        {
            Type = type;
        }

        public int NectarCount;
    }
}