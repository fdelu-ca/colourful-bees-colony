﻿using colourful_bees_colony.beesColony;
using colourful_bees_colony.graph;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace colourful_bees_colony.beesColony
{
    public class Hive
    {

        readonly int cntForagers;
        readonly int cntScout;
        Dictionary<Vertex, Stage> vertexStages = new Dictionary<Vertex, Stage>();
        Dictionary<int, int> result;//Iteration, CountColor
        int colorCount;

        public Hive(int cntScout, int cntForagers)
        {
            this.cntScout = cntScout;
            this.cntForagers = cntForagers;
        }
        public int TotalBees() { return cntScout + cntForagers; }

        internal int Start(Graph graph, int iterationsCount)
        {
            Graph bestGraph = (Graph)graph.Clone();
            Graph currGraph;
            colorCount = int.MaxValue;
            result = new Dictionary<int, int>();
            List<Bee> bees = new List<Bee>();
            int currIter = 0;
            /*
             * +1. Выбираем лучшие вершины и отправляем разведчиков 
             * +2. Выбираем смежные вершины и красим их свободные цвета
             * +3. Окрашиваем в допустимый цвет и выбираем новые
             * +4. Заносим вершину в обработанные
             * +5. Повторяем пп. 1-4 с вершинами которые не обработаны 
             * +6. Находим решение с заданным хроматическим числом
             * 7. Повторяем процесс по пунктам 1-6, до окончания итераций
             * +8. Каждые 20 итераций выдаём промежуточное значение количества цветов.
             */
            while (iterationsCount > currIter)//7
            {
                currGraph = (Graph)bestGraph.Clone();
               // Initialization();
               vertexStages = new Dictionary<Vertex, Stage>();
                do
                {
                    List<Bee> scouts = ProcessScouts(currGraph);
                    bees.AddRange(scouts);
                    //Распределение пчёл на участки
                    Dictionary<Vertex, double> bestVertices = DanceOfBees(scouts);
                    List<Bee> foragers = ProcessForagers(bestVertices,currGraph);
                    bees.AddRange(foragers);
                    //DebugPrintBees(bestVertices, iterationsCount,scouts, foragers, bees);
                    BeesUpdatedColor(currGraph, foragers);
                    BeesUpdatedColor(currGraph, scouts);

                    //DEBUG
                    //    Console.WriteLine();
                    //foreach (var item in vertexStages)
                    //    Console.Write(item.Key.Color.Name+"-");
                    //Console.WriteLine();
                    //Console.WriteLine(vertexStages.Count);
                } while (vertexStages.Count < currGraph.VertexCount);

                colorCount = currGraph.AllVertexColors().Count;
                //8
                Console.WriteLine($"currIter-{currIter}");
                if (currIter % 20 == 0)
                    result.Add(currIter,colorCount);
                currIter++;
                bees.Clear();
                //Сохранение результата
                if (colorCount <= bestGraph.AllVertexColors().Count)
                    bestGraph = currGraph;
            }
            return colorCount;
        }

            private void Initialization()
            {
               // for (int i = 0; i < vertexStages.Count; i++)
               //     vertexStages[vertexStages.ElementAt(i).Key] = Stage.Wait;
            }

            private void BeesUpdatedColor(Graph graph, List<Bee> bee)
        {
            Random rand = new Random();
            for (int i = 0; i < bee.Count; i++)
            {
                HashSet<Color> colorList = new HashSet<Color>();
                List<Vertex> neighbors = graph.GetVertexEdges(bee[i].Vertex);
                foreach (Vertex neighbor in neighbors)
                    colorList.Add(neighbor.Color);

                //Определение исключений в графах
                List<Color> allColor = graph.AllVertexColors();
                List<Color> ExceptColors = colorList.Except(allColor).ToList();
                ExceptColors.AddRange(allColor.Except(colorList).ToList());
                if (ExceptColors.Count > 0)
                    bee[i].Vertex.Color = ExceptColors.ElementAt(rand.Next(0, ExceptColors.Count - 1));
                else
                    bee[i].Vertex.Color = GraphBuilder.GetRandColor();

                //DEBUG
                //Console.WriteLine($"Id-{bee[i].Vertex.Id},Color-{bee[i].Vertex.Color.Name},Count-{allColor.Count}");
            }
        }

        private void DebugPrintBees(Dictionary<Vertex, double> bestVertices, int iterationsCount, List<Bee> scouts, List<Bee> foragers, List<Bee> bees)
        {
            int i = 0;
            double sum = 0;
            foreach (var item in bestVertices)
            {
                sum += item.Value;
                Console.WriteLine($"I-{i++},Key-{item.Key},Val-{item.Value},S-{sum}");
            }


            Console.WriteLine("Iteration: " + iterationsCount);
            Console.WriteLine("Scouts: " + scouts.Count);
            Console.WriteLine("Foragers: " + foragers.Count);
            Console.WriteLine("All Bees: " + bees.Count);
        }

        private List<Bee> ProcessForagers(Dictionary<Vertex, double> bestVertices, Graph graph)
        {
            List<Bee> bees = new List<Bee>();
            //3.1.1.	проверить все участки и выбрать наиболее перспективные 
            int usedBees;
            foreach (var kVert in bestVertices)
            {
                Vertex v = kVert.Key;
                List<Vertex> neighbors = graph.GetVertexEdges(v);

                //Установка количества феражиров не может быть больше количества соседей
                int neighborgCount = (int)(1.0 * kVert.Value * cntForagers);
                if (neighborgCount > neighbors.Count)
                    neighborgCount = neighbors.Count;
                usedBees = 0;

                for (int i = 0; usedBees < neighborgCount; i++)
                {
                    if (i >= neighborgCount && vertexStages.ContainsKey(neighbors[i]) && vertexStages[neighbors[i]] == Stage.Processed)
                    {
                        continue;
                    }
                    Bee bee = new Bee(BeeType.Foragers);
                    bee.Vertex = neighbors[i];
                    bee.NectarCount = graph.GetEdgeCount(neighbors[i]);
                    bees.Add(bee);
                    usedBees++;
                }
            }
            return bees;
        }

        private Dictionary<Vertex, double> DanceOfBees(List<Bee> scouts)
        {
            Dictionary<Vertex, double> result = new Dictionary<Vertex, double>();

            int nectarSum = 0; //Sum NB
            foreach (Bee item in scouts)
                nectarSum += item.NectarCount;

            foreach (Bee item in scouts)
            {
                double nectarPercent = 1.0 * item.NectarCount/nectarSum;
                result.Add(item.Vertex, nectarPercent);
            }

            return result.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        private List<Bee> ProcessScouts(Graph graph)
        {
            List<Bee> bees = new List<Bee>();

            for (int i = 0; i < cntScout; i++) //1
            {

                //Получаем данные от разведчика
                Bee bee = new Bee(BeeType.Scout);
                Vertex vertex = GraphBuilder.GetRandVertex(graph);
                if (vertexStages.Count >= graph.VertexCount)//Выход при достижении последней вершины
                    break;

                //Пропускаем уже обработанные вершины
                if (vertexStages.ContainsKey(vertex) && vertexStages[vertex] == Stage.Processed)
                {
                    i--;
                    continue;
                }

                //Добавляем информацию от разведчика
                bee.Vertex = vertex;
                bee.NectarCount = graph.GetEdgeCount(vertex);
                bees.Add(bee);

                //Отмечаем обработанную вершину
                vertexStages.Add(vertex, Stage.Processed);
            }
            return bees;
        }

        internal void PrintResult()
        {
            /*
                readonly int cntForagers;
                readonly int cntScout;
                Dictionary<Vertex, Stage> verStages = new Dictionary<Vertex, Stage>();
                Dictionary<int, int> result;//Iteration, CountColor
                int colorCount;
            
            */
            Console.WriteLine($"cntForagers: "+ cntForagers);
            Console.WriteLine($"cntScout: " + cntScout);
            int processedVertex = vertexStages.Where(x=>x.Value == Stage.Processed).Count();
            Console.WriteLine($"Processed Vertex: " + processedVertex);
            Console.WriteLine($"Color Count: " + colorCount);
            Console.WriteLine();
            Console.WriteLine();
            foreach (var item in result)
            {
                Console.WriteLine($"Iterations-{item.Key.ToString("000")}/tColors-{item.Value.ToString("000")}");
            }
            Console.WriteLine();

        }
    }
}